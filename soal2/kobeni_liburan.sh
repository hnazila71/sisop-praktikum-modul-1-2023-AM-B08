#0 */10 * * * bash ~/sisop/Soal2/kobeni_liburan.sh
cd ~/sisop/Soal2

jam_hour=$(date +"%H")
if [ $jam_hour == "00" ]
then
  jam_hour=1
fi

# cari berapa banyak nama folder yang mengandung keyword kumpulan
n=$(find -name '*kumpulan*' | wc -l) 

if [ $n == 0 ] # jika folder belum ada
then
  n=$((n+1))
  mkdir kumpulan_$n # buat folder pertama
else # jika sudah ada
  n=$((n+1))
  mkdir kumpulan_$n # buat folder selanjutnya
fi

for ((i=1; i<=$jam_hour; i=i+1))
do
  wget https://www.gramedia.com/blog/content/images/2022/08/1332769727.png -O kumpulan_$n/perjalanan_$i
done

# buat zip setiap 1 hari, berdasarkan crontab at 00:00
jam_hour=$(date +"%H")
if [ $jam_hour == "00" ]
then
  zip_ke=$(find -name '*devil*' | wc -l)
  for ((i=1; i<=n; i=i+1))
  do
    zip_ke=$((zip_ke+1))
    zip -rm devil_$zip_ke kumpulan_$i
  done
fi



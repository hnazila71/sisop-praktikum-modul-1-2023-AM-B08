# Meminta input username dan password dari pengguna
read -p "Masukkan username: " username
read -p "Masukkan password: " password

# Mencari username dan password pada file /users/users.txt
result=$(grep "^$username:" users/users.txt)

# Menentukan waktu
time=$(date +"%y/%m/%d %H:%M:%S")

# Memeriksa apakah username ditemukan dan password cocok
if [ -n "$result" ]; then
    stored_password=$(echo $result | cut -d ':' -f 2 | tr -d ' ')
    if [ "$password" = "$stored_password" ]; then
        echo "Selamat datang, $username!"
        echo "$time LOGIN: INFO User $username logged in" >> log.txt
    else
        echo "Password yang dimasukkan salah."
        echo "$time LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    fi
else
    echo "Login gagal: username $username tidak ditemukan."
    echo "$time LOGIN: ERROR Username $username not found" >> log.txt
fi
# Meminta input username dan password dari pengguna
read -p "Masukkan username: " username
read -p "Masukkan password: " password

time=$(date +"%y/%m/%d %H:%M:%S")

# Mencari apakah username sudah terdaftar atau belum
if grep -q "^$username:" "users/users.txt"; then
    echo "Username sudah terdaftar."
    echo "$time REGISTER: ERROR User already exists" >> log.txt

else

    # Menentukan kriteria password yang aman
    length=$(echo -n $password | wc -c)
    has_lower=$(echo -n $password | grep -c '[a-z]')
    has_upper=$(echo -n $password | grep -c '[A-Z]')
    has_digit=$(echo -n $password | grep -c '[0-9]')
    is_alphanumeric=$(echo -n $password | grep -c '^[[:alnum:]]*$')
    not_username=$(echo -n $password | grep -c "^$username$")
    contains_chicken=$(echo -n $password | grep -c 'chicken')
    contains_ernie=$(echo -n $password | grep -c 'ernie')

    # Memeriksa apakah password memenuhi kriteria yang ditentukan
    if [ $contains_chicken -eq 1 ] || [ $contains_ernie -eq 1 ]; then
        echo "Password tidak boleh menggunakan kata 'chicken' atau 'ernie'."
    elif [ $not_username -eq 1 ]; then
        echo "Password tidak boleh sama dengan username."
    elif [ $length -lt 8 ]; then
        echo "Password harus minimal 8 karakter."
    elif [ $has_lower -eq 0 ] || [ $has_upper -eq 0 ]; then
        echo "Password harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
    elif [ $is_alphanumeric -eq 0 ] || [ $has_digit -eq 0 ]; then
        echo "Password harus alphanumeric"

    else
        # Menambahkan username dan password ke dalam file /users/users.txt
        echo "$username:$password" >> users/users.txt
        echo "$time REGISTER: INFO User $username registered successfully" >> log.txt
        echo "Pendaftaran user berhasil!"
    fi
fi
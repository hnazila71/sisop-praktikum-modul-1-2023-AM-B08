echo "Top 5 University in Japan :"
awk '{
  if((n < 5) && (/Japan/)){
    print
    ++n
  }
}' "2023 QS World University Rankings.csv"

printf '\n'
echo "FSR Terendah :"
awk '{
  if((n < 5) && (/Japan/)){
    print
    ++n
  }
}' "2023 QS World University Rankings.csv" | sort -t, -nk9 | awk 'NR==1{print}'

printf '\n'
echo  "Top 10 University in Japan by ger-rank :"
awk '{
  if((n < 10) && (/Japan/)){
    print
    ++n
  }
}' "2023 QS World University Rankings.csv" | sort -t, -nk20

printf '\n'
echo "Universitas paling keren di dunia : "
awk '/Keren/ {print}' "2023 QS World University Rankings.csv"

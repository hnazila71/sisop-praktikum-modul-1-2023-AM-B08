#!/bin/bash
echo "Input nama file yang ingin di decrypt (hh:mm dd:mm:yyyy): "
read nama
#tiap alfabet pada enkripsi akan ditambah dengan variabel penambah
#yang mana penambah adalah jam dilakukannya enkripsi yang sudah ditambah di baris atas
penambah=$(awk 'NR==1{print}' "$nama.txt")

hour=$(date +"%H:%M %d:%m:%Y")
filename="Hasil Decrypt $hour.txt"

#ambil seluruh hasil enkripsi, kecuali pada baris pertama (yaitu hour penambahnya)
syslog="$(awk '{
  if((n == 0)){
    n++
  }
  else {
    print
    n++
  }
}' "$nama.txt")"

#proses dekripsi hampir sama dengan enkripsi, hanya saja alfabet dibalik z-a
reverse_abc=zyxwvutsrqponmlkjihgfedcbazyxwvutsrqponmlkjihgfedcba 
decrypt=$(echo "$syslog" | tr "${reverse_abc:0:26}" "${reverse_abc:${penambah}:26}")

echo "$decrypt" > "$filename"


